import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  persons: string[] = ['Manila', 'Quezon', 'Malabon', 'Paranaque'];

  onPersonCreated(personName: string) {
    this.persons.push(personName);
  }
}
